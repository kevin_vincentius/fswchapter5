const express = require("express");
const router = express.Router();
const app = express()

app.use(express.json());

router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

const logger = (req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
};

app.use(logger)

app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

app.use(function (req, res, next) {
  res.status(404).json({
    status: "fail",
    errorrs: "are you lost?",
  });
});

module.exports = router;
