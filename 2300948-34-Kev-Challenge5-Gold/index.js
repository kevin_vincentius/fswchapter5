const express = require("express")
const app = express()
const fs = require("fs")
const bodyParser = require("body-parser") 
let userlist = require("./db/userlist.json")

app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static("public"))
app.use(express.json())
app.set("view engine", "ejs")

//import modul
const router = require("./middleware/router");
app.use(router);

//middleware 
const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
  };
  app.use(logger);

//routing
app.get("/", function(req, res) {
    res.render("landingpage")
})

app.get("/game", function(req, res) {
    res.render("game")
})

app.get("/login", (req, res) => {
    res.render("login")
})

app.get("/signup", function(req, res) {
    res.render("signup")
})

app.get("/work", function(req, res) {
    res.render("work")
})

app.get("/contact", function(req, res) {
    res.render("contact")
})

app.get("/aboutme", function(req, res) {
    res.render("aboutme")
})

app.get("/home", function(req, res) {
    res.render("home")
})

// api login
app.get("/api/v1/userlist", (req, res) => {
    res.status(200).json(userlist);
})

app.post('/api/v1/login', (req, res) => {
    const { username, password } = req.body; 
    
    const userlist = JSON.parse(fs.readFileSync('./db/userlist.json'));
    if(userlist.find(userlist => userlist.username === username && userlist.password === password)){
        res.redirect("http://localhost:3000/home")
    } else {
        res.status(401).json({error: "invalid username/ password"})
    }
})

app.listen(3000, function(){
    console.log("server running on port 3000")
})

